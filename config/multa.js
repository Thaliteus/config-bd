var multa = function multa(x) {
    if(x < 60){
        return x*0.33;
    }
    if(x >= 60){
        return 20;
    }
}
module.exports = ()=>{
    return multa;
}