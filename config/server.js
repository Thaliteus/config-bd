fs = require('fs');
// Load The Path Module
path = require('path');

var confgs = require('./config');

config = JSON.parse(fs.readFileSync('config.json'));
var express           = require('../node_modules/express')
    ,cors             = require('../node_modules/cors')
    ,pdf              = require('../node_modules/express-pdf')
    ,bodyParser       = require('../node_modules/body-parser')
    ,expressValidator = require('../node_modules/express-validator')
    ,consign          = require('../node_modules/consign');

var app = express();
app.use(cors());
app.options('*', cors());

app.set('view engine', 'ejs');
app.set('views','app/views');
app.use(require('stylus').middleware(path.join(process.cwd(), 'public')));
app.use(express.static(path.join(process.cwd(), 'public')));

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}));
app.use(expressValidator());

//Carregar o modulo de pdf
app.use(pdf);

// Load Express Session Module
session = require('express-session');
app.use(session({ // Setup Session Middleware
  secret: config.session.secret,
  saveUninitialized: false,
  resave: false
}));

consign()
    .include('app/routes')
    .include('app/routes/iss')
    .include('app/routes/relatorios')
    .include('app/routes/acordo')
    .include('app/controlles')
    .include('app/model')
    .include('app/helpers')
    .include('config/dbConn.js')
    .include('config/dbSyncSql.js')
    .include('config/funcoes.js')
    .include('config/converteMoeda.js')
    .include('config/serverLog.js')
    .include('config/multa.js')
    .into(app);


// Load Connect Flash Module
// flash = require('connect-flash');
// app.use(flash());
// app.use(function(req, res, next) { // Setup Global Flash Message Middleware

//   res.locals.success_msg 	= req.flash('success_msg');
//   res.locals.error_msg 	= req.flash('error_msg');
//   res.locals.error 			= req.flash('error');
//   res.locals.user 			= req.user || null;
//   next();
// });

module.exports = app;
